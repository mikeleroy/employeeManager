-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Set 12, 2018 alle 16:56
-- Versione del server: 10.1.35-MariaDB
-- Versione PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employeemanagerdb`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `marital_status`
--

CREATE TABLE `marital_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `marital_status`
--

INSERT INTO `marital_status` (`id`, `name`) VALUES
(1, ' single'),
(2, 'married'),
(3, 'divorced'),
(5, 'widowed'),
(7, 'Re-Married');

-- --------------------------------------------------------

--
-- Struttura della tabella `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20180904105100');

-- --------------------------------------------------------

--
-- Struttura della tabella `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `role`
--

INSERT INTO `role` (`id`, `name`, `description`) VALUES
(1, 'user', 'a simple and standard user '),
(2, 'manager', ''),
(3, 'admin', 'user administrator website');

-- --------------------------------------------------------

--
-- Struttura della tabella `skill`
--

CREATE TABLE `skill` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `skill`
--

INSERT INTO `skill` (`id`, `name`) VALUES
(1, ' PHP'),
(2, 'JAVA'),
(3, 'C#'),
(6, 'ANDROID'),
(7, 'SWIFT'),
(8, 'ANGULAR'),
(9, 'REACT'),
(11, 'Javascript'),
(12, 'C++');

-- --------------------------------------------------------

--
-- Struttura della tabella `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `marital_id_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date NOT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `user`
--

INSERT INTO `user` (`id`, `marital_id_id`, `name`, `surname`, `username`, `password`, `country`, `avatar`, `birthday`, `role_id`) VALUES
(1, 1, 'Mauri', 'Mauricio', 'admin', '$2y$13$F2UcnoBK8/cAsl7RYM3F.extFUDuHi5pSdAITrkaK0rPHBlGkSC.e', 'Italia', '156cec9d031cefb86d342f2de2cfd4ed.jpeg', '2000-10-10', 3),
(2, 1, 'Michele ', 'Fornelli', 'mike', '$2y$13$WmcJO51/OlJWzC00PF0ZauQHSj2Jt08WF4lB4Wq5hZFc4AsXt4jXS', 'Italia', '4adbd7dd5283f8209f9003bc5857a825.png', '1989-02-09', 1),
(10, 1, 'Viviana', 'Vivi', 'vivi', '$2y$13$wNRLotvd8YsnhQBHNPlNnO4MNhvIuD0XhMk9VEsmYGsHJN3BQwx9O', 'Italia', '3e1d5c310e0e53049c8d3ebe3c3d5321.png', '1987-02-09', 1),
(11, 2, 'Claudio', 'Rota', 'rota', '$2y$13$P3lzW8rBXhHCcY81T1hvne40sU06xn35Ku/n50xBXFGMRJ2qoG87W', 'Germany', '1194a15c880e72040ef35bdbb92f8c22.png', '1980-06-10', 3),
(13, NULL, 'Virginia', 'Virginia', 'virgi', '$2y$13$jDk0.ZXP0xidVccsY5hT9OaPue48Xm4Kwy5PPZB03tD/PtyoXVqVa', 'Poland', NULL, '2018-02-28', 1),
(15, 2, 'demo', 'demo', 'demo', '$2y$13$QaAGBTdJNLMYErum4k/RWOuNYhYcX8iTA74VSBKIfmKKZG9CET5qq', 'Italia', NULL, '2018-09-24', 3),
(16, NULL, 'qwerty', 'qwerty', 'qwerty', '$2y$13$t.txFL1wo7DonamluUMpbOUJerfLtplxdFN1Bng9rVLEPAC0PcjsK', '', NULL, '2018-09-11', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `user_skill`
--

CREATE TABLE `user_skill` (
  `id` int(11) NOT NULL,
  `user_id_id` int(11) DEFAULT NULL,
  `skill_id_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `user_skill`
--

INSERT INTO `user_skill` (`id`, `user_id_id`, `skill_id_id`) VALUES
(50, 2, 1),
(53, 2, 6),
(54, 2, 7),
(95, 1, 1),
(100, 10, 1),
(101, 10, 2),
(102, 10, 3),
(105, 1, 2),
(106, 11, 12),
(108, 13, 6),
(110, 15, 1),
(111, 15, 11),
(112, 15, 7),
(113, 16, 1),
(114, 16, 8);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `marital_status`
--
ALTER TABLE `marital_status`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indici per le tabelle `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `skill`
--
ALTER TABLE `skill`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `UNIQ_8D93D649C746DF48` (`marital_id_id`) USING BTREE,
  ADD KEY `UNIQ_8D93D649D60322AC` (`role_id`) USING BTREE;

--
-- Indici per le tabelle `user_skill`
--
ALTER TABLE `user_skill`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BCFF1F2F9D86650F` (`user_id_id`),
  ADD KEY `IDX_BCFF1F2F5A6C0D6B` (`skill_id_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `marital_status`
--
ALTER TABLE `marital_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `skill`
--
ALTER TABLE `skill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT per la tabella `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT per la tabella `user_skill`
--
ALTER TABLE `user_skill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D649C746DF48` FOREIGN KEY (`marital_id_id`) REFERENCES `marital_status` (`id`),
  ADD CONSTRAINT `FK_8D93D649D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Limiti per la tabella `user_skill`
--
ALTER TABLE `user_skill`
  ADD CONSTRAINT `FK_BCFF1F2F5A6C0D6B` FOREIGN KEY (`skill_id_id`) REFERENCES `skill` (`id`),
  ADD CONSTRAINT `FK_BCFF1F2F9D86650F` FOREIGN KEY (`user_id_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
