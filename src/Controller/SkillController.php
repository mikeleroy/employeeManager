<?php

namespace App\Controller;

use App\Entity\MaritalStatus;
use App\Entity\Role;
use App\Entity\Skill;
use App\Entity\UserSkill;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\Tools\Pagination\Paginator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\File\File;

class SkillController extends Controller
{
    private $encoder;
    private $userRepo;
    private $skillRepo;
    private $userSkillRepo;
    private $entityManager;

    /**
     * SkillController constructor.
     */
    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager)
    {
        $this->encoder = $encoder;
        $this->entityManager = $entityManager;
        $this->userRepo = $entityManager->getRepository(User::class);
        $this->skillRepo = $entityManager->getRepository(Skill::class);
        $this->userSkillRepo = $entityManager->getRepository(UserSkill::class);

    }


    /**
     * @Route("/skill/dashboard", name="skill_dashboard")
     */
    public function dashboard()
    {
        $skills = $this->skillRepo->findAll();
        return $this->render('skill/dashboard.html.twig', ['skills' => $skills]);
    }


    /**
     * @Route("/skill/dashdemo", name="dashdemo")
     */
    public function dashdemo(Request $request){
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT a FROM AcmeMainBundle:Article a";
        $query = $em->createQuery($dql);

        $skills = $this->skillRepo->findAll();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $skills, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            3/*limit per page*/
        );

        // parameters to template
        return $this->render('skill/dashdemo.html.twig', ['pagination' => $pagination]);

    }

    /**
     *
     * @Route("/skill/edit/{id}", name="skill_edit")
     */
    public function edit($id)
    {


        if ($id==0)
        {
            $skill = new Skill();
        }
        else{
            $skill = $this->skillRepo->find($id);

        }

        return $this->render('skill/edit.html.twig', [ 'skill' => $skill]);
    }



    /**
     * @Route("/skill/update", name="skill_update")
     */
    public function update(Request $request)
    {
        $code = $request->get('code');


        if ($code == 0 || $code == null) {

            $skill = new Skill();
            $skill->setName($request->get('name'));



        } else {
            $skill = $this->skillRepo->find($code);
            $skill->setName( $request->get('name'));
        }





        $this->entityManager->persist($skill);
        $this->entityManager->flush();

        $this->addFlash(
            'notice',
            'The skill ' . $skill->getName() .' has correctly been updated'
        );

        return $this->redirectToRoute('skill_dashboard');


    }


    /**
     * @Route("/skill/skillSearch", name="skill_search")
     */
    public function skillSearch(Request $request)
    {
        $query = $request->get('query');

        $skills = $this->skillRepo->findByName($query);

        return $this->render('skill/search.html.twig', ['skills' => $skills]);
    }



    /**
     * @Route("/skill/delete", name="skill_delete")
     */
    public function deleteSkill(Request $request) {

        //if skill is not used control

        $code = $request->get('id');

        $skill = $this->skillRepo->find($code);

        //remove skill on table userskill into db
        $personalSkills = $this->userSkillRepo->findBySkill($skill->getId());
        foreach ($personalSkills as $ps) {
            $this->entityManager->remove($ps);
            $this->entityManager->flush();
        }

        $this->entityManager->remove($skill);
        $this->entityManager->flush();

        $data = 1;

       return new JsonResponse($data);

    }





}