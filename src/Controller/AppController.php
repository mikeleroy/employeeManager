<?php

namespace App\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\MaritalStatus;
use App\Entity\Role;
use App\Entity\Skill;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

use App\Entity\UserSkill;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\File\File;
class AppController extends Controller
{

    private $encoder;
    private $userRepo;
    private $skillRepo;
    private $userSkillRepo;
    private $entityManager;
    private $authChecker;
    private $security;
    /**
     * AppController constructor.
     */
    public function __construct(UserPasswordEncoderInterface $encoder, Security $security ,EntityManagerInterface $entityManager,AuthorizationCheckerInterface $authChecker)
    {
        $this->encoder = $encoder;
        $this->entityManager = $entityManager;
        $this->userRepo = $entityManager->getRepository(User::class);
        $this->skillRepo = $entityManager->getRepository(Skill::class);
        $this->userSkillRepo = $entityManager->getRepository(UserSkill::class);
        $this->authChecker = $authChecker;
        $this->security = $security;


    }



    /**
     * @Route("/app/index" , name="index")
     */
    public function index()
    {

        return $this->render('app/index.html.twig', []);
    }


    /**
     *
     * @Route("/app/settings", name="account_settings")
     */
    public function settings()
    {
//        return the view page settings (only for change password)
        return $this->render('app/settings.html.twig');
    }



    /**
     * @Route("/app/settingsUpdate", name="settings_update")
     */
    public function update(Request $request)
    {
        //        update the user password

        $code = $request->get('code');

        $user = $this->userRepo->find($code);
        $oldPass = $request->get('oldPass');
        $encodedOld = $this->encoder->encodePassword($user, $oldPass);

        if ($encodedOld == $user->getPassword()) {
            $newPass = $request->get('newPass');
            $encodedNew = $this->encoder->encodePassword($user, $newPass);

            $user->setPassword($encodedNew);

            $this->entityManager->persist($user);
            $this->entityManager->flush();
            return $this->redirectToRoute('user_dashboard');

        }

        return $this->redirectToRoute('user_dashboard');

    }

    /**
     * @Route("/app/savePassword", name="save_password")
     */
    public function savePassword(Request $request)
    {

//        function for save the password RESET
        $code = $request->get('code');



        $user = $this->userRepo->find($code);

        $newPass = $request->get('newPass');

        if ($user != null && $newPass != null) {

            $encodedNew = $this->encoder->encodePassword($user, $newPass);

            $user->setPassword($encodedNew);

            $this->entityManager->persist($user);
            $this->entityManager->flush();
            return $this->redirectToRoute('login');

        }
        else
        {

            return $this->redirectToRoute('reset_password');

        }


    }


    /**
     * @Route("/app/resetPassword", name="reset_password")
     */
    public function resetPassword(Request $request)
    {

//      function for check if user exist (username + country combination check)

        $username = $request->get('username');
        $country = $request->get('country');

        $user = $this->userRepo->findByUsernameCountry($username,$country);

        if($user != null)
        {
            return $this->render('app/resetPassword.html.twig', ['user' => $user]);

        }
        else
        {
            $this->addFlash(
                'notice',
                'Username and country combination incorrect'
            );
            return $this->render('app/forgotPassword.html.twig');

        }



    }


    /**
     * @Route("/app/forgotPassword", name="forgot_password")
     */
    public function forgotPassword(Request $request)
    {
//        return the view page for request the reset password (1° step check if user exist by combiantion username + country)
        return $this->render('app/forgotPassword.html.twig');

    }


    /**
     * @Route("/admin")
     */
    public function admin()
    {

        $user = $this->security->getUser()->getRole()->getName();
        var_dump($user);die;
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) {
        throw new AccessDeniedException('Unable to access this page!');
        }

        return new Response('<html><body>Admin page!</body></html>');
    }

}