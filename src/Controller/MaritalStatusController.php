<?php

namespace App\Controller;

use App\Entity\MaritalStatus;
use App\Entity\Role;
use App\Entity\Skill;
use App\Entity\UserSkill;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\File\File;

class MaritalStatusController extends Controller
{
    private $encoder;
    private $userRepo;
    private $maritalStatusRepo;
    private $entityManager;

    /**
     * MaritalStatus constructor.
     */
    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager)
    {
        $this->encoder = $encoder;
        $this->entityManager = $entityManager;
        $this->userRepo = $entityManager->getRepository(User::class);
        $this->maritalStatusRepo = $entityManager->getRepository(MaritalStatus::class);

    }


    /**
     * @Route("/maritalStatus/dashboard", name="marital_dashboard")
     */
    public function dashboard()
    {
        $maritalStatus = $this->maritalStatusRepo->findAll();
        return $this->render('maritalStatus/dashboard.html.twig', ['maritalStatus' => $maritalStatus]);
    }


    /**
     *
     * @Route("/martialStatus/edit/{id}", name="marital_edit")
     */
    public function edit($id)
    {


        if ($id==0)
        {
            $marital = new MaritalStatus();
        }
        else{
            $marital = $this->maritalStatusRepo->find($id);

        }

        return $this->render('maritalStatus/edit.html.twig', ["marital" => $marital]);
    }



    /**
     * @Route("/maritalStatus/update", name="marital_update")
     */
    public function update(Request $request)
    {
        $code = $request->get('code');



        if ($code == 0 || $code == null) {

            $marital = new MaritalStatus();
            $marital->setName($request->get('name'));



        } else {
            $marital = $this->maritalStatusRepo->find($code);
            $marital->setName( $request->get('name'));
        }


        $this->entityManager->persist($marital);
        $this->entityManager->flush();


        $this->addFlash(
            'notice',
            'The status ' . $marital->getName() .' has correctly been updated'
        );

        return $this->redirectToRoute('marital_dashboard');


    }


    /**
     * @Route("/maritalStatus/delete", name="marital_delete")
     */
    public function deleteMarital(Request $request) {

        //if skill is not used control

        $code = $request->get('id');

        $marital = $this->maritalStatusRepo->find($code);

        //remove skill on table userskill into db
        $users = $this->userRepo->findByMarital($marital->getId());
        foreach ($users as $u) {
            $u->setMaritalId(null);

        }

        $this->entityManager->remove($marital);
        $this->entityManager->flush();

        $data = 1;

        return new JsonResponse($data);

    }



}