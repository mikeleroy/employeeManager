<?php

namespace App\Repository;

use App\Entity\UserSkill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserSkill|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserSkill|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserSkill[]    findAll()
 * @method UserSkill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserSkillRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserSkill::class);
    }

    public function findByUser($value)
    {
        return $this->createQueryBuilder('u')
            ->where('u.user_id = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    //find the USERID by skill -> used for deleted a skill
    public function findBySkill($value)
    {
        return $this->createQueryBuilder('us')
            ->join('us.user_id', 'u')
            ->where('us.skill_id = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }



    public function findByUserSkill($user,$skill)
    {
        return $this->createQueryBuilder('u')
            ->where('u.user_id = :user')
            ->andWhere('u.skill_id = :skill')
            ->setParameter('user', $user)
            ->setParameter('skill', $skill)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }


    public function findByCode($value)
    {
        return $this->createQueryBuilder('u')
            ->where('u.skill_id = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }






//    /**
//     * @return UserSkill[] Returns an array of UserSkill objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserSkill
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
